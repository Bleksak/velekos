disk_save_info:
	pusha
	mov byte [bootdev], dl

	mov ah, 0x8
	int 0x13

	jc disk_error

	and cx, 00111111b

	mov [SectorsPerTrack], cx

	movzx dx, dh
	inc dx

	mov [Sides], dx

	popa
	ret
; Get file list on floppy
; IN/OUT AX = location to store string

disk_get_file_list:
	pusha

	mov word [.file_list_tmp], ax

	mov eax, 0			; Needed for some older BIOSes

	call disk_reset_floppy		; Just in case disk was changed

	mov ax, 19			; Root dir starts at logical sector 19
	call disk_lba_to_chs

	mov si, disk_buffer		; ES:BX should point to our buffer
	mov bx, si

	mov ah, 2			; Params for int 13h: read floppy sectors
	mov al, 14			; And read 14 of them

	pusha				; Prepare to enter loop

.read_root_directory:
	popa
	pusha

	stc
	int 13h				; Read sectors
	call disk_reset_floppy	; Check we've read them OK
	jnc .show_dir_init		; No errors, continue

	call disk_reset_floppy		; Error = reset controller and try again
	jnc .read_root_directory
	popa
	jmp .done			; Double error, exit 'dir' routine

.show_dir_init:
	popa

	xor ax, ax
	mov si, disk_buffer		; Data reader from start of filenames

	mov word di, [.file_list_tmp]	; Name destination buffer

.start_entry:
	mov al, [si+11]			; File attributes for entry
	cmp al, 0Fh 		; Windows marker, skip it
	je .skip

	test al, 18h			; Is this a directory entry or volume label?
	jnz .skip			; Yes, ignore it

	mov al, [si]
	cmp al, 229			; If we read 229 = deleted filename
	je .skip

	cmp al, 0			; 1st byte = entry never used
	je .done


	mov cx, 1			; Set char counter
	mov dx, si			; Beginning of possible entry

.testdirentry:
	inc si
	mov byte al, [si]			; Test for most unusable characters
	cmp al, ' '			; Windows sometimes puts 0 (UTF-8) or 0FFh
	jl .nxtdirentry
	cmp al, '~'
	ja .nxtdirentry

	inc cx
	cmp cx, 11			; Done 11 char filename?
	je .gotfilename
	jmp .testdirentry


.gotfilename:				; Got a filename that passes testing
	mov si, dx			; DX = where getting string

	xor cx, cx
.loopy:
	mov byte al, [si]
	cmp al, ' '
	je .ignore_space
	mov byte [di], al
	inc si
	inc di
	inc cx
	cmp cx, 8
	je .add_dot
	cmp cx, 11
	je .done_copy
	jmp .loopy

.ignore_space:
	inc si
	inc cx
	cmp cx, 8
	je .add_dot
	jmp .loopy

.add_dot:
	mov byte [di], '.'
	inc di
	jmp .loopy

.done_copy:
	mov byte [di], ','		; Use comma to separate filenames
	inc di

.nxtdirentry:
	mov si, dx			; Start of entry, pretend to skip to next

.skip:
	add si, 32			; Shift to next 32 bytes (next filename)
	jmp .start_entry


.done:
	dec di
	mov byte [di], 0		; Zero-terminate string (gets rid of final comma)

	popa
	ret


	.file_list_tmp		dw 0

disk_reset_floppy:
	push ax
	push dx
	mov ax, 0
; ******************************************************************
	mov dl, [bootdev]
; ******************************************************************
	stc
	int 13h
	pop dx
	pop ax
	ret

disk_error:
	ret


;In = 
;AX: File name
;CX: Position to load at
;Out= 
;BX: File size in bytes, Carry flag set if not found

disk_load_file:
	
	call str_uppercase

	mov [.filename], ax
	mov [.ram_pos], cx

	mov eax, 0

	call disk_reset_floppy
	jnc .floppy_ok

	jmp $
	;TODO

.floppy_ok:

	mov ax, 19

	call disk_lba_to_chs

	mov si, disk_buffer
	mov bx, si

	mov ah, 0x2
	mov al, 14
	pusha

.read_root_dir:
	popa
	pusha

	stc
	int 0x13
	jnc .search_root_dir

	call disk_reset_floppy
	jnc .read_root_dir

	popa
	jmp .root_problem
	;TODO

.search_root_dir:
	popa
	mov cx, 224
	mov bx, -32
.next_entry:
	
	add bx, 32
	mov di, disk_buffer
	add di, bx

	mov al, [di]

	cmp al, 0
	je .root_problem

	cmp al, 229
	je .next_entry

	mov al, [di+11]

	cmp al, 0xF
	je .next_entry

	test al, 0x18
	jnz .next_entry

	mov byte [di+11], 0

	mov ax, di
	call os_print_4hex

	mov di, disk_buffer
	add di, bx

	call os_print_newline

	mov ax, di

	call os_print_4hex

	call os_print_newline

	;call str_uppercase

	mov si, [.filename]

	call strcmp

	je .file_found

	loop .next_entry

.file_found:
	;mov si, .filename
	;call os_print
	mov al, 'K'
	call os_cprint
	ret


.root_problem:
	stc
	mov bx, 0
	ret

	.cluster dw 0
	.filename dw 0
	.filesize dw 0
	.ram_pos dw 0



disk_lba_to_chs:
	push bx
	push ax

	mov bx, ax

	xor dx, dx
	div word [SectorsPerTrack]
	inc dl
	mov cl, dl
	mov ax, bx

	xor dx, dx

	div word [SectorsPerTrack]

	xor dx, dx

	div word [Sides]
	mov dh, dl
	mov ch, al


	pop ax
	pop bx

	mov byte dl, [bootdev]

	ret


SectorsPerTrack dw 18
Sides dw 2
bootdev db 0