

os_command_line:
	call os_clear_screen

	mov si, OS_NAME
	call os_print

	mov si, OS_VERSION
	call os_print

get_command:
	mov di, OS_COMMAND_INPUT
	mov al, 0
	mov cx, 256

	rep stosb

	mov di, OS_COMMAND
	mov cx, 32

	rep stosb

	mov si, prompt_sign
	call os_print

	mov ax, OS_COMMAND_INPUT

	call os_input_string

	call os_print_newline

	mov ax, OS_COMMAND_INPUT
	call str_chomp

	mov si, OS_COMMAND_INPUT
	cmp byte [si], 0
	je get_command

	mov word [param_list], di
	mov si, OS_COMMAND_INPUT
	mov di, OS_COMMAND

	call str_copy

	mov ax, OS_COMMAND_INPUT
	call str_uppercase

	mov si, OS_COMMAND_INPUT


; --------------------------


	mov di, cmd_shutdown
	call strcmp
	je command_shutdown



; --------------------------

	mov di, cmd_help
	call strcmp
	je command_help


; --------------------------

	mov di, cmd_reboot
	call strcmp
	je sys_reboot

; ---------------------------

	mov di, cmd_clear
	call strcmp
	je command_clear

; ----------------------------

	mov di, cmd_dir
	call strcmp
	je command_dir

; ---------------------------

	mov di, cmd_time
	call strcmp
	je command_time

; ----------------------------
	mov di, cmd_date
	call strcmp
	je command_date

; -----------------------------
	mov di, cmd_cat
	call strcmp
	je command_cat

; -----------------------------
	mov di, cmd_del
	call strcmp
	je command_del

; ------------------------------

	mov di, cmd_copy
	call strcmp
	je command_copy

; ---------------------------------

	mov di, cmd_rename
	call strcmp
	je command_rename

; ---------------------------------

	mov di, cmd_size
	call strcmp
	je command_size

; ---------------------------------

	mov di, cmd_version
	call strcmp
	je command_version

; ---------------------------------
	


	ret

command_shutdown:
command_help:
command_clear:
command_dir:
command_time:
command_date:
command_cat:
command_del:
command_copy:
command_rename:
command_size:
command_version:


param_list dw 0

OS_NAME db "VELEK OS", 0
OS_VERSION db "1.0", 0

OS_COMMAND_INPUT times 256 db 0
OS_COMMAND times 32 db 0


prompt_sign db ">", 0

cmd_clear db "CLEAR", 0
cmd_dir db "DIR", 0
cmd_time db "TIME", 0
cmd_date db "DATE", 0
cmd_cat db "CAT", 0
cmd_del db "DEL", 0
cmd_copy db "COPY", 0
cmd_rename db "REN", 0
cmd_size db "SIZE", 0
cmd_version db "VERSION", 0
cmd_help db "HELP", 0
cmd_reboot db "REBOOT", 0
cmd_shutdown db "SHUTDOWN", 0

cmd_name_text db "Velek OS 1.0", 13, 10, 0
cmd_help_text db "Available commands: CLEAR, DIR, TIME, DATE, CAT, DEL, COPY, REN, SIZE, VERSION, HELP, REBOOT, SHUTDOWN", 13, 10, 0