;in SI, DI
;carry flag set if equal

strcmp:
	mov al, [si]
	mov bl, [di]

	cmp al, bl

	jne .difference

	cmp al, 0

	je .done

	inc si

	inc di

	jmp strcmp

	.done:
	stc
	ret

	.difference:
	clc
	ret

;AL = char to find
;SI = string

;returns AX = char position
;			SI = string at char pos

str_find_char:
	.loop:

	mov cx, 0

	mov bl, [si]
	
	or bl, bl

	jz .done

	inc si
	inc cx

	cmp al, bl

	jne .loop

	stc
	mov ax, cx
	ret

	.done:
	clc
	ret

;in = SI = string
;AL = char to find
;BL = char to replace with

str_replace_char:
	call str_find_char

	jc .more

	ret

	.more:

	mov [si], bl

	ret

;in/out AX = string

str_uppercase:
	pusha
	mov si, ax

	.loop:

		cmp byte [si], 0
		je .done

		cmp byte [si], 'a'
		jb .noatoz

		cmp byte [si], 'z'
		ja .noatoz

		sub byte [si], 0x20


	.noatoz:

	inc si

	jmp .loop

	.done:

	popa
	ret


;in/out AX = string

str_lowercase:
	pusha
	mov si, ax

	.loop:

	cmp byte [si], 0
	je .done

	cmp byte [si], 0x41
	jb .noatoz

	cmp byte [si], 0x5A
	ja .noatoz

	add byte [si], 0x20

	.noatoz:

	inc si
	jmp .loop

	.done:

	popa
	ret


; in AX = string
;out CX = length

str_len:
	pusha

	mov bx, ax			; Move location of string to BX

	mov cx, 0			; Counter

.more:
	cmp byte [bx], 0		; Zero (end of string) yet?
	je .done
	inc bx				; If not, keep adding
	inc cx
	jmp .more


.done:
	mov word [.tmp_counter], cx	; Store count before restoring other registers
	popa

	mov ax, [.tmp_counter]		; Put count back into AX before returning
	ret


	.tmp_counter	dw 0

;in SI = string

str_reverse:
	
	cmp byte[si], 0
	je .done

	mov ax, si
	call str_len

	cmp cx, 1
	je .done

	mov di, si
	add di, cx
	dec di

	.loop:

		mov byte al, [si]
		mov byte bl, [di]

		mov byte [si], bl
		mov byte [di], al

		inc si
		dec di

		cmp di, si
		jne .loop

	.done:

	popa
	ret

;SI = SOURCE
;DI = DESTINATION

str_copy:
	pusha
	.loop:

	cmp byte [si], 0
	je .done

	mov al, byte[si]

	mov [di], al

	inc si
	inc di

	jmp .loop

	.done:

	popa
	ret


;IN - SI = string
;	AX = character count

str_truncate:
	add si, ax

	mov byte [si], 0
	ret

;IN - AX, BX
;OUT - CX

str_join:
	pusha


	mov si, ax
	mov di, cx

	call str_copy

	call str_len

	add cx, ax

	mov si, bx
	mov di, cx

	call str_copy

	.done:
	popa
	ret

; str_chomp -- Strip leading and trailing spaces from a string
; IN: AX = string location

str_chomp:
	pusha

	mov dx, ax			; Save string location

	mov di, ax			; Put location into DI
	mov cx, 0			; Space counter

.keepcounting:				; Get number of leading spaces into BX
	cmp byte [di], ' '
	jne .counted
	inc cx
	inc di
	jmp .keepcounting

.counted:
	cmp cx, 0			; No leading spaces?
	je .finished_copy

	mov si, di			; Address of first non-space character
	mov di, dx			; DI = original string start

.keep_copying:
	mov al, [si]			; Copy SI into DI
	mov [di], al			; Including terminator
	cmp al, 0
	je .finished_copy
	inc si
	inc di
	jmp .keep_copying

.finished_copy:
	mov ax, dx			; AX = original string start

	call str_len
	cmp ax, 0			; If empty or all blank, done, return 'null'
	je .done

	mov si, dx
	add si, ax			; Move to end of string

.more:
	dec si
	cmp byte [si], ' '
	jne .done
	mov byte [si], 0		; Fill end spaces with 0s
	jmp .more			; (First 0 will be the string terminator)

.done:
	popa
	ret

;Delete char from a string
; IN - SI = string, AL = char to remove

str_strip:
	pusha

	mov di, si
	mov bl, al

	.next_character:
		lodsb
		stosb
		
		cmp al, 0
		je .done

		cmp al, bl
		jne .next_character

	.skip:
	dec di
	jmp .next_character


	.done:
	popa
	ret

;See if 2 strings match up to certain number of chars
;IN SI = String 1, DI = String 2, CL = chars to check
;OUT - Carry flag set if same, clear if different

str_strincmp:
	pusha

	.more:
	mov al, [si]
	mov bl, [di]

	cmp al, bl
	jne .not_same

	cmp al, 0
	je .same

	inc si
	inc di

	dec cl
	cmp cl, 0
	je .same

	jmp .more

	.not_same:
	popa
	clc
	ret
	.same:
	popa
	stc
	ret

;Take a string and parse it into words
;in = SI - String
;out = AX, BX, CX, DX - Strings

str_parse:
	push si

	mov ax, si			; AX = start of first string

	mov bx, 0			; By default, other strings start empty
	mov cx, 0
	mov dx, 0

	push ax				; Save to retrieve at end

.loop1:
	lodsb				; Get a byte
	cmp al, 0			; End of string?
	je .finish
	cmp al, ' '			; A space?
	jne .loop1
	dec si
	mov byte [si], 0		; If so, zero-terminate this bit of the string

	inc si				; Store start of next string in BX
	mov bx, si

.loop2:					; Repeat the above for CX and DX...
	lodsb
	cmp al, 0
	je .finish
	cmp al, ' '
	jne .loop2
	dec si
	mov byte [si], 0

	inc si
	mov cx, si

.loop3:
	lodsb
	cmp al, 0
	je .finish
	cmp al, ' '
	jne .loop3
	dec si
	mov byte [si], 0

	inc si
	mov dx, si

.finish:
	pop ax

	pop si
	ret


;IN: SI = String
;OUT: AX = int
str_to_int:
	pusha

	mov ax, si
	call str_len

	;AX = str len

	mov cx, ax

	add si, ax

	xor bx, bx

	xor ax, ax

	mov word [.multiplier], 1

	.loop:
		xor ax, ax
		mov byte al, [si]
		sub al, 0x30

		mul word [.multiplier]

		add bx, ax

		mov word ax, [.multiplier]
		mov dx, 0xA
		mul dx

		mov word [.multiplier], ax

		dec cx
		cmp cx, 0
		je .done
		dec si
		jmp .loop

	.done:
		mov word [.tmp], bx
		popa
		mov word ax, [.tmp]
		ret


	.tmp dw 0
	.multiplier dw 0
;IN : AX = unsigned int
;OUT: AX = string

int_to_str:
	pusha

	mov cx, 0
	mov bx, 10			; Set BX 10, for division and mod
	mov di, .t			; Get our pointer ready

.push:
	mov dx, 0
	div bx				; Remainder in DX, quotient in AX
	inc cx				; Increase pop loop counter
	push dx				; Push remainder, so as to reverse order when popping
	test ax, ax			; Is quotient zero?
	jnz .push			; If not, loop again
.pop:
	pop dx				; Pop off values in reverse order, and add 48 to make them digits
	add dl, '0'			; And save them in the string, increasing the pointer each time
	mov [di], dl
	inc di
	dec cx
	jnz .pop

	mov byte [di], 0		; Zero-terminate string

	popa
	mov ax, .t			; Return location of string
	ret


	.t times 7 db 0

;IN/OUT = AX

sint_to_str:
	pusha

	mov cx, 0
	mov bx, 10			; Set BX 10, for division and mod
	mov di, .t			; Get our pointer ready

	test ax, ax			; Find out if X > 0 or not, force a sign
	js .neg				; If negative...
	jmp .push			; ...or if positive
.neg:
	neg ax				; Make AX positive
	mov byte [.t], '-'		; Add a minus sign to our string
	inc di				; Update the index
.push:
	mov dx, 0
	div bx				; Remainder in DX, quotient in AX
	inc cx				; Increase pop loop counter
	push dx				; Push remainder, so as to reverse order when popping
	test ax, ax			; Is quotient zero?
	jnz .push			; If not, loop again
.pop:
	pop dx				; Pop off values in reverse order, and add 48 to make them digits
	add dl, '0'			; And save them in the string, increasing the pointer each time
	mov [di], dl
	inc di
	dec cx
	jnz .pop

	mov byte [di], 0		; Zero-terminate string

	popa
	mov ax, .t			; Return location of string
	ret


	.t times 7 db 0

;IN : DX:AX = long integer
;OUT : DI = String

long_to_string:
	pusha

	mov si, di			; Prepare for later data movement

	mov word [di], 0		; Terminate string, creates 'null'

	cmp bx, 37			; Base > 37 or < 0 not supported, return null
	ja .done

	cmp bx, 0			; Base = 0 produces overflow, return null
	je .done

.conversion_loop:
	mov cx, 0			; Zero extend unsigned integer, number = CX:DX:AX
					; If number = 0, goes through loop once and stores '0'

	xchg ax, cx			; Number order DX:AX:CX for high order division
	xchg ax, dx
	div bx				; AX = high quotient, DX = high remainder

	xchg ax, cx			; Number order for low order division
	div bx				; CX = high quotient, AX = low quotient, DX = remainder
	xchg cx, dx			; CX = digit to send

.save_digit:
	cmp cx, 9			; Eliminate punctuation between '9' and 'A'
	jle .convert_digit

	add cx, 'A'-'9'-1

.convert_digit:
	add cx, '0'			; Convert to ASCII

	push ax				; Load this ASCII digit into the beginning of the string
	push bx
	mov ax, si
	call str_len		; AX = length of string, less terminator
	mov di, si
	add di, ax			; DI = end of string
	inc ax				; AX = nunber of characters to move, including terminator

.move_string_up:
	mov bl, [di]			; Put digits in correct order
	mov [di+1], bl
	dec di
	dec ax
	jnz .move_string_up

	pop bx
	pop ax
	mov [si], cl			; Last digit (LSD) will print first (on left)

.test_end:
	mov cx, dx			; DX = high word, again
	or cx, ax			; Nothing left?
	jnz .conversion_loop

.done:
	popa
	ret

; os_string_tokenize -- Reads tokens separated by specified char from
; a string. Returns pointer to next token, or 0 if none left
; IN: AL = separator char, SI = beginning; OUT: DI = next token or 0 if none

str_tokenize:
	push si

.next_char:
	cmp byte [si], al
	je .return_token
	cmp byte [si], 0
	jz .no_more
	inc si
	jmp .next_char

.return_token:
	mov byte [si], 0
	inc si
	mov di, si
	pop si
	ret

.no_more:
	mov di, 0
	pop si
	ret