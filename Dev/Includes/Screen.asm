;OUT : DH = X
;	 : DL = Y

os_get_cursor_pos:
	
	pusha

	mov ah, 0x3

	int 0x10

	.done:

	mov [.tmp], dx

	popa

	mov dx, [.tmp]

	ret

	.tmp dw 0

;IN : DH = X
;	: DL = Y

os_set_cursor_pos:

	pusha
	
	xor bl, bl
	mov ah, 0x2

	int 0x10

	.done:
	popa
	ret

os_reset_cursor_pos:
	
	push dx

	xor dx, dx

	call os_set_cursor_pos

	.done:
	pop dx
	ret

os_show_cursor:
	pusha
	mov ch, 00000110b
	mov cl, 00000111b
	mov ah, 1

	int 0x10

	popa
	ret

os_hide_cursor:
	pusha

	mov ch, 00100000b
	mov ah, 1
	int 0x10

	popa
	ret

os_clear_screen:

	mov ax, 0x3

	int 0x10

	call os_reset_cursor_pos

	.done:
	ret

;IN - AL = character

os_cprint:
	mov ah, 0xE
	int 0x10

	ret

;IN = SI = String position
os_print:
	pusha
	.start:
	
	lodsb
	or al, al

	je .done

	call os_cprint

	jmp .start

	.done:
	popa
	ret


os_print_space:
	push ax
	mov al, 0x20

	call os_cprint

	pop ax

	ret
os_print_newline:
	
	push ax
	mov al, key_newline

	call os_cprint

	mov al, key_enter
	call os_cprint
	
	pop ax
	ret

; os_print_digit -- Displays contents of AX as a single digit
; Works up to base 37, ie digits 0-Z
; IN: AX = "digit" to format and print

os_print_digit:
	pusha

	cmp ax, 9			; There is a break in ASCII table between 9 and A
	jle .digit_format

	add ax, 'A'-'9'-1		; Correct for the skipped punctuation

.digit_format:
	add ax, '0'			; 0 will display as '0', etc.	

	call os_cprint

	popa
	ret

os_print_1hex:
	pusha

	and ax, 0Fh			; Mask off data to display

	call os_print_digit

	popa
	ret

os_print_2hex:

	pusha

	push ax
	shr ax, 4
	call os_print_1hex

	pop ax

	call os_print_1hex

	popa
	ret

os_print_4hex:
	
	pusha

	push ax
	mov al, ah
	call os_print_2hex

	pop ax
	call os_print_2hex

	popa
	ret

;IN/OUT AX: LOCATION OF STRING
;MAX 255 CHARACTERS

os_input_string:
	pusha

	mov di, ax
	xor cx, cx

	.loop:

	call os_keyboard_wait


	cmp al, key_enter
	je .done

	cmp al, key_backspace
	je .backspace

	call os_cprint
	stosb
	inc cx

	cmp cx, 254
	jae .done

	jmp .loop


	inc cx
	inc di

	.backspace:

	cmp cx, 0
	je .loop

	call os_get_cursor_pos		; Backspace at start of screen line?
	cmp dl, 0
	je .backspace_linestart

	call os_get_cursor_pos
	dec dl
	call os_set_cursor_pos
	call os_print_space
	call os_set_cursor_pos

	dec cx
	dec di

	jmp .loop


	.backspace_linestart:
	dec dh
	mov dl, 79

	call os_set_cursor_pos

	dec cx
	dec di

	jmp .loop

	.done:
	xor al, al
	stosb

	popa
	ret

os_dump_string:
	pusha

	mov bx, si

	.line:
		mov di, si
		mov cx, 0
	.more_hex:
		lodsb
		cmp al, 0
		je .chr_print

		call os_print_2hex
		call os_print_space		; Single space most bytes
		inc cx

		cmp cx, 8
		jne .q_next_line

		call os_print_space		; Double space centre of line
		jmp .more_hex

	.q_next_line:
		cmp cx, 16
		jne .more_hex

	.chr_print:
		call os_print_space

		mov al, '|'	

		call os_cprint
		call os_print_space

		mov si, di			; Go back to beginning of this line
		mov cx, 0

	.more_chr:
		lodsb
		cmp al, 0
		je .done

		cmp al, ' '
		jae .tst_high

		jmp short .not_printable

	.tst_high:
		cmp al, '~'
		jbe .output

	.not_printable:
		mov al, '.'

	.output:
		call os_cprint

		inc cx
		cmp cx, 16
		jl .more_chr

		call os_print_newline		; Go to next line
		jmp .line

	.done:
		call os_print_newline		; Go to next line

	popa
	ret

os_dump_registers:
	pusha

	call os_print_newline

	push di
	push si
	push dx
	push cx
	push bx

	mov si, .ax_string
	call os_print
	call os_print_4hex

	call os_print_newline

	pop ax

	mov si, .bx_string
	call os_print
	call os_print_4hex

	call os_print_newline

	pop ax

	mov si, .cx_string
	call os_print
	call os_print_4hex

	call os_print_newline

	pop ax

	mov si, .dx_string
	call os_print
	call os_print_4hex

	call os_print_newline

	pop ax

	mov si, .si_string
	call os_print
	call os_print_4hex

	call os_print_newline

	pop ax

	mov si, .di_string
	call os_print
	call os_print_4hex

	call os_print_newline

	popa

	ret

	.ax_string		db 'AX: ', 0
	.bx_string		db 'BX: ', 0
	.cx_string		db 'CX: ', 0
	.dx_string		db 'DX: ', 0
	.si_string		db 'SI: ', 0
	.di_string		db 'DI: ', 0