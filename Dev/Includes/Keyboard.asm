%DEFINE scan_leftarrow 0x4b
%DEFINE scan_uparrow 0x48
%DEFINE scan_downarrow 0x50
%DEFINE scan_rightarrow 0x4d


%DEFINE key_nul 0x00
%DEFINE key_soh 0x01
%DEFINE key_stx 0x02
%DEFINE key_etx 0x03
%DEFINE key_eot 0x04
%DEFINE key_enq 0x05
%DEFINE key_ack 0x06
%DEFINE key_bel 0x07
%DEFINE key_backspace 0x08
%DEFINE key_tab 0x09
%DEFINE key_newline 0xA
%DEFINE key_vt 0xB
%DEFINE key_ff 0xC
%DEFINE key_enter 0xD
%DEFINE key_so 0xE
%DEFINE key_si 0xF
%DEFINE key_dle 0x10
%DEFINE key_dc1 0x11
%DEFINE key_dc2 0x12
%DEFINE key_dc3 0x13
%DEFINE key_dc4 0x14
%DEFINE key_nak 0x15
%DEFINE key_syn 0x16
%DEFINE key_etb 0x17
%DEFINE key_can 0x18
%DEFINE key_em 0x19
%DEFINE key_sub 0x1A
%DEFINE key_escape 0x1B
%DEFINE key_fs 0x1C
%DEFINE key_gs 0x1D
%DEFINE key_rs 0x1E
%DEFINE key_us 0x1F
%DEFINE key_space 0x20
%DEFINE key_exclamation_mark 0x21
%DEFINE key_quotation_mark 0x22
%DEFINE key_number_sign 0x23
%DEFINE key_dollar_sign 0x24
%DEFINE key_percentage 0x25
%DEFINE key_ampersand 0x26
%DEFINE key_apostrophe 0x27
%DEFINE key_left_round_bracket 0x28
%DEFINE key_right_round_bracket 0x29
%DEFINE key_asterisk 0x2A
%DEFINE key_plus_sign 0x2B
%DEFINE key_comma 0x2C
%DEFINE key_hyphon 0x2D
%DEFINE key_dot 0x2E
%DEFINE key_slash 0x2F
%DEFINE key_0 0x30
%DEFINE key_1 0x31
%DEFINE key_2 0x32
%DEFINE key_3 0x33
%DEFINE key_4 0x34
%DEFINE key_5 0x35
%DEFINE key_6 0x36
%DEFINE key_7 0x37
%DEFINE key_8 0x38
%DEFINE key_9 0x39
%DEFINE key_colon 0x3A
%DEFINE key_semicolon 0x3B
%DEFINE key_left_guillemets 0x3C
%DEFINE key_equals_sign 0x3D
%DEFINE key_right_guillemets 0x3E
%DEFINE key_question_mark 0x3F
%DEFINE key_at_sign 0x40
%DEFINE key_A 0x41
%DEFINE key_B 0x42
%DEFINE key_C 0x43
%DEFINE key_D 0x44
%DEFINE key_E 0x45
%DEFINE key_F 0x46
%DEFINE key_G 0x47
%DEFINE key_H 0x48
%DEFINE key_I 0x49
%DEFINE key_J 0x4A
%DEFINE key_K 0x4B
%DEFINE key_L 0x4C
%DEFINE key_M 0x4D
%DEFINE key_N 0x4E
%DEFINE key_O 0x4F
%DEFINE key_P 0x50
%DEFINE key_Q 0x51
%DEFINE key_R 0x52
%DEFINE key_S 0x53
%DEFINE key_T 0x54
%DEFINE key_U 0x55
%DEFINE key_V 0x56
%DEFINE key_W 0x57
%DEFINE key_X 0x58
%DEFINE key_Y 0x59
%DEFINE key_Z 0x5A
%DEFINE key_left_square_bracket 0x5B
%DEFINE key_backslash 0x5C
%DEFINE key_right_square_bracket 0x5D
%DEFINE key_caret 0x5E
%DEFINE key_underscore 0x5F
%DEFINE key_backtick 0x60
%DEFINE key_a 0x61
%DEFINE key_b 0x62
%DEFINE key_c 0x63
%DEFINE key_d 0x64
%DEFINE key_e 0x65
%DEFINE key_f 0x66
%DEFINE key_g 0x67
%DEFINE key_h 0x68
%DEFINE key_i 0x69
%DEFINE key_j 0x6A
%DEFINE key_k 0x6B
%DEFINE key_l 0x6C
%DEFINE key_m 0x6D
%DEFINE key_n 0x6E
%DEFINE key_o 0x6F
%DEFINE key_p 0x70
%DEFINE key_q 0x71
%DEFINE key_r 0x72
%DEFINE key_s 0x73
%DEFINE key_t 0x74
%DEFINE key_u 0x75
%DEFINE key_v 0x76
%DEFINE key_w 0x77
%DEFINE key_x 0x78
%DEFINE key_y 0x79
%DEFINE key_z 0x7A
%DEFINE key_left_curly_bracket 0x7B
%DEFINE key_vertical_bar 0x7C
%DEFINE key_right_curly_bracket 0x7D
%DEFINE key_tilde 0x7E
%DEFINE key_delete 0x7F


os_keyboard_wait:
	pusha
	mov ah, 0x10

	xor ax, ax

	int 0x16

	mov [.tmp], ax

	.done:
	popa
	mov ax, [.tmp]
	ret

	.tmp dw 0


os_keyboard_check:
	pusha

	xor ax, ax

	mov ah, 0x1
	int 0x16

	jz .done

	mov ax, 0
	int 0x16

	mov [.tmp], ax

	popa

	mov ax, [.tmp]

	ret

	.done:

	popa
	mov ax, 0
	ret

	.tmp dw 0