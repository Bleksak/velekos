bits 16

	disk_buffer	equ	24576

os_vectors:

;		MAIN

	jmp os_main

;		SCREEN

	jmp os_clear_screen
	jmp os_reset_cursor_pos
	jmp os_set_cursor_pos
	jmp os_get_cursor_pos

	jmp os_show_cursor
	jmp os_hide_cursor

	jmp os_cprint
	jmp os_print

	jmp os_print_space
	jmp os_print_newline

	jmp os_show_cursor
	jmp os_hide_cursor

	jmp os_print_space
	jmp os_print_newline

	jmp os_print_digit

	jmp os_print_1hex
	jmp os_print_2hex
	jmp os_print_4hex

	jmp os_input_string

	jmp os_dump_string
	jmp os_dump_registers

;		KEYBOARD

	jmp os_keyboard_wait
	jmp os_keyboard_check

;		STRING

	jmp strcmp
	jmp str_find_char
	jmp str_replace_char
	jmp str_uppercase
	jmp str_lowercase
	jmp str_len
	jmp str_reverse
	jmp str_copy
	jmp str_truncate
	jmp str_join
	jmp str_chomp
	jmp str_strip
	jmp str_strincmp
	jmp str_parse
	jmp str_to_int
	jmp int_to_str
	jmp long_to_string
	jmp str_tokenize

;		TIME

	jmp os_time_set_fmt
	jmp os_get_time_string
	jmp os_set_date_fmt
	jmp os_get_date_string

;		MATH

;		COMMAND LINE INTERFACE

	jmp os_command_line

	

%INCLUDE "Dev/Includes/Keyboard.asm"
%INCLUDE "Dev/Includes/Screen.asm"
%INCLUDE "Dev/Includes/Disk.asm"
%INCLUDE "Dev/Includes/String.asm"
%INCLUDE "Dev/Includes/System.asm"
%INCLUDE "Dev/Includes/CLI.asm"
%INCLUDE "Dev/Includes/Time.asm"



os_main:
	
	;cli				; Clear interrupts
	;mov ax, 0
	;mov ss, ax			; Set stack segment and pointer
	;mov sp, 0FFFFh
	;sti				; Restore interrupts

	cld				; The default direction for string operations
					; will be 'up' - incrementing address in RAM



	.SegmentChange:

	mov ax, 0x1000
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	.DriveInfo:


	mov byte [bootdev], dl
	push es
	mov ah, 8
	int 0x13
	pop es
	and cx, 00111111b
	mov word [SectorsPerTrack], cx
	movzx dx, dh
	inc dx
	mov [Sides], dx


	.Kernel:

	call os_clear_screen

	call disk_get_file_list

	mov si, ax

	call os_print

	call os_print_newline


	mov ax, filename

	call disk_load_file

	


	hlt

filename db "TESTAPP BIN", 0