bits 16

jmp short os_boot_start
nop

OEMLabel		db "VLEKBOOT"	; Disk label
BytesPerSector		dw 512
SectorsPerCluster	db 1
ReservedForBoot		dw 1
NumberOfFats		db 2
RootDirEntries		dw 224
				
TotalSectors	dw 2880
MediaDescriptorType db 0xF0
SectorsPerFat	 dw 9
SectorsPerTrack dw 18
Sides dw 2
HiddenSectors dd 0

LargeSectors		dd 0		; Number of LBA sectors
DriveNo			dw 0		; Drive No: 0
Signature		db 41		; Drive signature: 41 for floppy
VolumeID		dd 00000000h	; Volume ID: any number
VolumeLabel		db "VELEKOS    "; Volume Label: any 11 chars
FileSystem		db "FAT12   "	; File system type: don't change!

os_boot_start:
	
	;mov ax, 07C0h			; Set up 4K of stack space above buffer
	;add ax, 544			; 8k buffer = 512 paragraphs + 32 paragraphs (loader)
	;cli				; Disable interrupts while changing stack
	;mov ss, ax
	;mov sp, 4096
	;sti				; Restore interrupts


	mov ax, 0x07C0
	mov ds, ax

	cmp dl, 0

	je .no_change
	mov [boot_device], dl
	mov ah, 0x8
	int 0x13        ; dl = boot device
					; dx = Sides - 1
					; cx = Sectors per track - 1

	jc disk_error
					;FROM HERE

	and cx, 00111111b

	mov [SectorsPerTrack], cx
	
	movzx dx, dh
	inc dx

	mov [Sides], dx
	
.no_change:
	xor eax, eax

	mov ax, 19

	call calculate_head_track_sector

	mov si, disk_buffer

	mov bx, ds
	mov es, bx
	mov bx, si

	mov ah, 2
	mov al, 14

	pusha

	.read_root:

	popa
	pusha

	stc
	int 0x13

	jnc search_file
	call reset_floppy
	jnc .read_root

	jmp $

	jmp reboot


search_file:
	
	popa


	mov ax, ds			; Root dir is now in [buffer]
	mov es, ax			; Set DI to this info

	mov di, disk_buffer

	mov cx, word [RootDirEntries]
	xor ax, ax

.nextEntry:
	xchg cx, dx

	mov si, kernel_filename
	mov cx, 11
	rep cmpsb
	je .file_found

	add ax, 32

	mov di, disk_buffer		;TODO - SHORTEN
	add di, ax

	xchg dx, cx
	loop .nextEntry

	jmp reboot	

.file_found:
	mov ax, word [es:di + 0xF]
	mov word [cluster], ax
	
	mov ax, 1
	call calculate_head_track_sector
	mov di, disk_buffer
	mov bx, di

	mov ah, 2
	mov al, [SectorsPerFat]

	pusha
.read_fat:
	popa
	pusha

	stc

	int 0x13

	jnc read_fat_ok
	call reset_floppy
	jnc .read_fat

	jmp disk_error

read_fat_ok:
	popa

	mov ax, 0x1000
	mov es, ax
	xor bx, bx
	mov ah, 2
	mov al, 1

	push ax


load_file_sector:
	mov ax, word [cluster]
	add ax, 31

	call calculate_head_track_sector

	mov ax, 0x1000
	mov es, ax

	mov bx, word [kernel_pointer]

	pop ax
	push ax

	stc
	int 0x13

	jnc next_cluster

	call reset_floppy
	jmp load_file_sector


next_cluster:

	mov ax, [cluster]
	mov dx, 0
	mov bx, 3
	mul bx
	mov bx, 2
	div bx				; DX = [cluster] mod 2
	mov si, disk_buffer
	add si, ax			; AX = word in FAT for the 12 bit entry
	mov ax, word [ds:si]

	or dx, dx			; If DX = 0 [cluster] is even; if DX = 1 then it's odd

	jz .even				; If [cluster] is even, drop last 4 bits of word
					; with next cluster; if odd, drop first 4 bits

.odd:
	shr ax, 4			; Shift out first 4 bits (they belong to another entry)
	jmp short next_cluster_cont


.even:
	and ax, 0FFFh			; Mask out final 4 bits


next_cluster_cont:
	mov word [cluster], ax		; Store cluster

	cmp ax, 0FF8h			; FF8h = end of file marker in FAT12
	jae end

	add word [kernel_pointer], 512		; Increase buffer pointer 1 sector length
	jmp load_file_sector


end:					; We've got the file to load!
	pop ax				; Clean up the stack (AX was pushed earlier)
	mov dl, byte [boot_device]		; Provide kernel with boot device info

	call 0x1000:0			; Jump to entry point of loaded kernel!
hlt
disk_error:
	mov si, msg_disk_error
	call print_string
	jmp reboot


hlt


print_string:				
			; Output string in SI to screen
	pusha

	mov ah, 0Eh			; int 10h teletype function

.repeat:
	lodsb				; Get char from string
	cmp al, 0
	je .done			; If char is zero, end of string
	int 10h				; Otherwise, print it
	jmp short .repeat

.done:
	popa
	ret


reset_floppy:
	push ax
	push dx
	xor ax, ax
	mov dl, byte [boot_device]
	stc
	int 0x13
	pop dx
	pop ax
	ret


reboot:
	db 0x0ea
	dw 0x0
	dw 0xffff
	jmp $

calculate_head_track_sector:

	push bx
	push ax

	mov bx, ax

	xor dx, dx

	div word [SectorsPerTrack]

	inc dl

	mov cl, dl

	mov ax, bx

	xor dx, dx

	div word [SectorsPerTrack]

	xor dx, dx

	div word [Sides]

	mov dh, dl
	mov ch, al

	pop ax
	pop bx

	mov dl, byte [boot_device]
	
	ret

cluster dw 0
boot_device dw 0
kernel_pointer dw 0


kernel_filename db "KERNEL  BIN"


msg_disk_error db "Fatal disk error", 0


times 510-($-$$) db 0
dw 0x55AA

disk_buffer: