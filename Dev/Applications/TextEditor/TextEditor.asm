text_editor_launch:
	
	call os_clear_screen
	jmp text_editor_loop

text_editor_save_as:

text_editor_save:

text_editor_open_file:
	

text_editor_loop:
	mov di, buffer

	xor cl, cl
	.loop:
	call os_keyboard_check

	jz .loop

	cmp al, key_enter

	je .enter

	cmp al, key_backspace

	je .backspace

	cmp al, key_escape

	je .escape

	cmp ah, scan_leftarrow
	je .leftArrow

	cmp ah, scan_rightarrow
	je .rightArrow

	cmp ah, scan_uparrow
	je .upArrow

	cmp ah, scan_downarrow
	je .downArrow

	stosb

	call os_cprint

	inc cl
	jmp .loop


	.escape:

	jmp text_editor_exit

	
	.backspace:

		call sys_moveleft

		mov al, key_space

		call os_cprint

		call sys_moveleft

		dec di
		dec cl

		jmp .loop


	.leftArrow:

	call sys_moveleft

	jmp .loop

	.rightArrow:
	call sys_moveright

	jmp .loop

	.upArrow:
	call sys_moveup
	jmp .loop

	.downArrow:
	call sys_movedown
	jmp .loop


	.enter:
		
		call sys_newline

		mov al, key_newline
		stosb

	jmp text_editor_loop


text_editor_exit:
	jmp os_main