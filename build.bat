DEL /F Build\Image.img
DEL /F Bin\BootLoader.bin
DEL /F Bin\Kernel.bin

imdisk -D -m P:

nasm -f bin -o Bin/BootLoader.bin Dev/BootLoader.asm

nasm -f bin -o Bin/Kernel.bin Dev/Kernel.asm

nasm -f bin -o Bin/TestApp.bin Dev/RandomApp.asm

imdisk -a -f Build/Image.img -s 1440K -m P: -p "/FS:FAT /Y"
imdisk -D -m P:

rem dd if="Bin/BootLoader.bin" of="Build/Image.img" bs=512 count=1
pause

imdisk -a -f Build/Image.img -m P:
pause
copy Bin\Kernel.bin P:\
copy Bin\TestApp.bin P:\
copy Tische.txt P:\
pause
imdisk -D -m P:




pause